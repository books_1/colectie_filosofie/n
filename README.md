# N

## Content

```
./Nae Ionescu:
Nae Ionescu - Nelinistea metafizica.pdf

./Neagu Djuvara:
Neagu Djuvara - Civilizatii si tipare istorice.pdf

./Newton da Costa:
Newton da Costa - Logici clasice si neclasice.pdf

./Niall Ferguson:
Niall Ferguson - Civilizatia.pdf

./Niccolo Machiavelli:
Niccolo Machiavelli - Arta razboiului.pdf
Niccolo Machiavelli - Principele.pdf

./Nicholas Georgescu Roegen:
Nicholas Georgescu Roegen - Legea entropiei si procesul economic.pdf

./Nicolae Bagdasar:
Nicolae Bagdasar - Teoria cunostintei.pdf

./Nicolae Margineanu:
Nicolae Margineanu - Psihologie logica si matematica.pdf

./Nicolae Rambu:
Nicolae Rambu - Ratiunea speculativa in filosofia lui Hegel.pdf

./Nicolae Trandafoiu:
Nicolae Trandafoiu - Substanta si cauzalitatea in interpretarea empirismului englez.pdf

./Nicolae Turcan:
Nicolae Turcan - Cioran sau excesul ca filosofie.pdf
Nicolae Turcan - Credinta ca filosofie.pdf
Nicolae Turcan - Dumnezeul gandurilor marunte.pdf
Nicolae Turcan - Inceputul suspiciunii.pdf

./Nicolai Hartmann:
Nicolai Hartmann - Vechea si noua ontologie.pdf

./Nicolaus Cusanus:
Nicolaus Cusanus - De docta ignorantia.pdf
Nicolaus Cusanus - Pacea intre religii.pdf

./Niels Bohr:
Niels Bohr - Fizica atomica si cunoasterea umana.pdf
Niels Bohr - Teoria atomica si descrierea naturii.pdf

./Nigel Warburton:
Nigel Warburton - Cum sa gandim corect si eficient.pdf

./Nikolai Berdiaev:
Nikolai Berdiaev - Despre menirea omului.pdf
Nikolai Berdiaev - Filosofia lui Dostoievski.pdf

./Norberto Bobbio:
Norberto Bobbio - Dreapta si stanga.pdf
Norberto Bobbio - Liberalism si democratie.pdf

./Norbert Wiener:
Norbert Wiener - Sunt matematician.pdf

./Normand Baillargeon:
Normand Baillargeon - Mic curs de autoaparare intelectuala.pdf
```

